# Sublime Text Setup

1. Install [Package Control](https://packagecontrol.io/installation) for Sublime Text 3.
2. Copy the folders in this repository to the "Packages" directory (found in `AppData/Roaming/Sublime Text 3`).
